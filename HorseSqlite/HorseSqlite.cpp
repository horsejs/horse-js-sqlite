
#include <wx/dlimpexp.h>
#include <wx/stdpaths.h>
#include <wx/dir.h>
#include "HorseSqlite.h"
#ifndef DLL_EXPORT
#define DLL_EXPORT  WXEXPORT
#endif

extern "C"
{
    int callback(void* cb, int argc, char** argv, char** azColName) {
        int i;
        for (i = 0; i < argc; i++) {
            printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        }
        printf("\n");
        std::function<void(std::string&)>& method = (std::function<void(std::string&)>&)cb;
        std::string result = "���Թ�����";
        method(result);
        return 0;
    }

    std::string DLL_EXPORT init(std::string& paramStr, std::function<void(std::string&)> cb)
    {
        json paramObj = json::parse(paramStr);
        json result;
        result["success"] = true;
        auto openWhenLoad = paramObj["openWhenLoad"].get<bool>();
        if (!openWhenLoad) {
            result["success"] = true;
            return result.dump();
        }
        auto dbFileAtAppDataFolder = paramObj["dbFileAtAppDataFolder"].get<bool>();
        wxString dbFolderPath;
        if (dbFileAtAppDataFolder) {
            auto appName = paramObj["__appConfig"]["appName"].get<std::string>();
            auto appDataPath = wxStandardPaths::Get().GetUserConfigDir();
            dbFolderPath = appDataPath + L"/" + wxString::FromUTF8(appName);
            bool flag = wxDir::Exists(dbFolderPath);
            if (!flag) {
                wxDir::Make(dbFolderPath);
            }
        }
        else
        {
            dbFolderPath = paramObj["dbFolderPath"].get<std::string>();
        }
        auto dbFileName = paramObj["dbFileName"].get<std::string>();
        auto dbPath = dbFolderPath + L"/" + wxString::FromUTF8(dbFileName);        
        char* zErrMsg = 0;
        int rc;
        rc = sqlite3_open(dbPath.ToUTF8(), &db);
        if (rc) {
            result["success"] = false;
            result["error"] = "�����ݿ�ʧ��";
        }
        auto resultStr = result.dump();
        return resultStr;
    }
    std::string DLL_EXPORT execute(std::string& paramStr, std::function<void(std::string&)> cb)
    {
        json paramObj = json::parse(paramStr);
        json result;
        result["success"] = true;
        std::string sqlStr = paramObj["sql"].get<std::string>();
        const char* sql = sqlStr.c_str();
        bool async = paramObj["__async"].get<bool>();
        char* zErrMsg = 0;
        int rc;
        if (async) {
            rc = sqlite3_exec(db, sql, callback, &cb, &zErrMsg);
        }
        else
        {
            rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
        }
        if (rc != SQLITE_OK) {
            result["success"] = false;
            result["error"] = std::string(zErrMsg);
            sqlite3_free(zErrMsg);
        }
        auto resultStr = result.dump();
        return resultStr;
    }
    std::string DLL_EXPORT unload(std::string& paramObj, std::function<void(std::string&)> callback)
    {
        sqlite3_close(db);
        json result;
        result["success"] = true;
        return result.dump();
    }


}