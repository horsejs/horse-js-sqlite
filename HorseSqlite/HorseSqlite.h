#pragma once
#include <wx/wxprec.h>
#include <wx/wx.h>
#include <wx/defs.h>
#include <functional>
#include "json.hpp"
#include "sqlite/sqlite3.h"
using nlohmann::json;

#ifdef __WXMSW__
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT
#endif

sqlite3 * db;
namespace GeneralFuncs
{
	class MyGeneralFuncs
	{
	public:
		static DLL_EXPORT std::string init(std::string& paramStr, std::function<void(std::string&)> callback);
		static DLL_EXPORT std::string execute(std::string& paramStr, std::function<void(std::string&)> callback);
		static DLL_EXPORT std::string unload(std::string& paramStr, std::function<void(std::string&)> callback);
	};
}