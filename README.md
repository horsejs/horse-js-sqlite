本项目是 HorseJs 的 Sqlite 插件，该插件会随 HorseJs 一起发布，所以本仓储不再单独发布。

目前本插件只允许在同一时刻持有一个数据库连接。

PS： 如果你的项目不使用本地数据库，那么可以删除本插件的二进制文件，以减小 HorseJs 的体积，本插件在不同系统下对应的动态链接库文件名如下所示：

- HorseSqlite.dll（Windows 操作系统）
- HorseSqlite.so（Mac 操作系统）
- HorseSqlite.dylib（Linux 操作系统）
